﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour
{
    Vector3 startPosition;
    public GameObject sheetDown;
    public GameObject sheetRaised;


    void Start()
    {
        startPosition = gameObject.transform.position;
    }

    void Update()
    {

        if (Input.GetMouseButton(0))
        {
            transform.position = Input.mousePosition;
            sheetDown.SetActive(false);
            sheetRaised.SetActive(true);
        }
                else
                {
                    transform.position = startPosition;
                    sheetDown.SetActive(true);
                    sheetRaised.SetActive(false);
                }
        }

    }

